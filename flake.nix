{
  description = "The Syllabus of 420-317-AH";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
     flake-utils.lib.eachDefaultSystem (system:
       let
         pkgs = nixpkgs.legacyPackages.${system};
         git-hash = if (self ? rev) then builtins.substring 0 8 self.rev else "dirty";
         last-updated = if (self ? lastModified) then builtins.toString self.lastModified else "0";
         syllabus-fonts = pkgs.stdenv.mkDerivation {
          name = "syllabus-fonts";
          buildInputs = [pkgs.font-awesome pkgs.fira-code pkgs.bakoma_ttf];
          src =  builtins.filterSource (path: type: false) ./.;
          buildPhase =  ''
            mkdir build
            cp -r ${pkgs.font-awesome}/share/fonts/opentype/* build
            cp -r ${pkgs.fira-code}/share/fonts/truetype/* build
            cp -r ${pkgs.bakoma_ttf}/share/fonts/truetype/* build
            '';
          installPhase =  ''mv build $out'';
         };
        syllabus = pkgs.stdenv.mkDerivation {
            name = "syllabus";
            src = ./.;
            buildInputs = with pkgs; [typst syllabus-fonts yq git];
            buildPhase = ''
                export TYPST_FONT_PATHS=${syllabus-fonts}
                export LAST_UPDATED_STRING=`date -u +"%Y-%m-%dT%H:%M:%SZ"  -d @${last-updated}`
                tomlq --toml-output  ".language = \"fr\" | .githash = \"${git-hash}\" | .lastUpdated = \"$LAST_UPDATED_STRING\"" src/main.toml > src/main-tmp.toml
                cp src/main-tmp.toml src/main.toml
                ${pkgs.typst}/bin/typst compile src/main.typ syllabus.pdf
                '';
            installPhase =  ''
                mkdir $out
                cp src/main.toml $out
                mv syllabus.pdf $out/plan-de-cours-420-317-ah.pdf
                '';
          };
        # syllabus-en = syllabus "en";
       in
       {
         packages = {
          syllabus-fonts = syllabus-fonts;
          default = syllabus;
         };
         devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [typst
                                      syllabus-fonts
                                      yq];
            shellHook = '' 
            export TYPST_FONT_PATHS=${syllabus-fonts}
            '';
         };
       }
     );
}
