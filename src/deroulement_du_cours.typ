
#let deroulement-du-cours = [

=== Première partie : Initiation 

- Installation de l'environement de development: nodejs, deno, git, vscode et webstorm.
- Introduction à JavaScript et TypeScript
- Création d'un premier serveur web.
- Vue d'ensemble des dépendences JS Modules
- Projet 1 


=== Deuxième partie : Les interactions avec les bases de données 

- Server web avec Express et les API REST 
- Bases de données (MongoDB) 
- Projet 2 

=== Troisième partie: Optimisation du backend et authentification 

- JWT 
- Authentification et login 
- Node.js Raspberry Pi - GPIO 
- Projet 3 

]