#let _ponderation = link("https://www.cegepsquebec.ca/nos-cegeps/presentation/systeme-scolaire-quebecois/grille-de-cours-et-ponderation/")[Pondération]

#let frontpage = [ 
#align(center)[
    #image("logo-ahuntsic.png", width: 33%),

    #text(size: 20pt)[Plan De Cours]

    #text(size: 12pt)[Automn 2023]
]

#table(
  columns: (auto, 1fr),
  inset: 25pt,
  align: horizon,
  [Titre du cours],[*Développement d’applications de supervision et de monitorage*],
  [Code],[*420-317-AH*],
  _ponderation, text(weight: "bold", font: "FreeMono")[1-3-3],
  [Compétences visées], list(
        [*AF57 - Effectuer le développement d'applications Web transactionnelles*],
    ),
  [Programme], [*AEC Internet des Objets et Intelligence Artificielle*],
  [Enseignant], [
    - *Didier Amyot*
    - #link("mailto:didier.amyot@collegeahuntsic.qc.ca")[didier.amyot\@collegeahuntsic.qc.ca]
    ],
  [Département], [*Informatique*]
)
]

