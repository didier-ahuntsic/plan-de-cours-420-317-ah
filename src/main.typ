#import "frontpage.typ": frontpage
#import "generalites.typ": generalites
#import "evaluations.typ": evaluations
#import "deroulement_du_cours.typ": deroulement-du-cours
#import "course_bibliography.typ": course-bibliography
#let params = toml("main.toml")

#set page(
  footer: [
    #set align(center)
    #set text(8pt)
    Page #counter(page).display(
      "1 de 1",
      both: true,
    ) #linebreak()
    #text(size: 7pt, font: "Fira Code")[Dernière mise-à-jour: #params.at(default: "", "lastUpdated") | Identification: #params.at(default: "", "githash")]
  ]
)


#frontpage

#pagebreak()


#generalites

== Déroulement du cours

#deroulement-du-cours


#evaluations

#course-bibliography