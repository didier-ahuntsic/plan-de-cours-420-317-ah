#let generalites = [
== Présentation générale

Après ce cours, l'étudiant sera capable d'écrire une application
coté serveur en typescript et de la connecter à une base de 
données.


=== Objectif-standard visé
À l’issue de ce cours, l’étudiant sera en mesure de 
programmer une application de supervision et de 
monitorage dans un système embarqué afin de l’intégrer
 à un objet défini par le programmeur. 


=== Présentation du cours
Ce cours du 4#super[e] bloc a pour but d’initier 
l’étudiant au développement d’applications de 
supervision et de monitoring qui sera intégré sur 
un système d’objets intelligents. Il fait suite aux 
cours portant sur le développement d’application Web 
et d'objets intelligents et il prépare l’étudiant 
au cours portant sur le développement d’applications 
multiplateformes. 

]